﻿namespace testAPI.Controllers;

using Microsoft.AspNetCore.Mvc;
using System.Reactive.Subjects;
using System.Security.Claims;
using testAPI.Authorization;
using testAPI.Entities;
using testAPI.Models;
using testAPI.Services;

[ApiController]
[Authorize]
[Route("[controller]")]
public class UsersController : ControllerBase
{
    private IUserService _userService;
    private IUserFriendService _userFriendService;

    public UsersController(IUserService userService, IUserFriendService userfriend)
    {
        _userService = userService;
        _userFriendService = userfriend;
    }


    [AllowAnonymous]
    [HttpPost("authenticate")]
    public IActionResult Authenticate(AuthenticateRequest model)
    {
        var response = _userService.Authenticate(model);

        if (response == null)
            return BadRequest(new { message = "Username or password is incorrect" });

        return Ok(response);
    }

    [HttpGet]
    public IActionResult GetAll()
    {
        var users = _userService.GetAll();
        return Ok(users);
    }
    [HttpGet ("getmyid")]
    public ActionResult<string> GetMyId()
    {
        var userId = int.Parse(User.Claims.First(x => x.Type == "id").Value);
        if (userId != null)
        {
            return Ok(userId);
        }
        return BadRequest(new { message = "No claims!" });
    }


    /*public IActionResult GetFriend()
    {
        var usersFriend = _userFriendService.GetFriend();
        return Ok(usersFriend);
    }*/
}
